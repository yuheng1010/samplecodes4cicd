import React, {useState} from 'react';
import {calResult} from './utils/TellMeResult';

/**
 * @param {string} expected the answer of the quiz
 * @return {div} the quiz view component
 */
export default function GuessGameComponent({expected = '1234'}) {
  const [input, setInput] = useState('');
  const [guess, setGuess] = useState('快猜吧');

  const submit = (event) => {
    event.preventDefault();
    setGuess(calResult(input, expected));
    setInput('');
  };

  return (
    <div>
      <form onSubmit={submit}>
        <input
          value={input}
          onChange={(event) => setInput(event.target.value)}
          required
          type="text"
          placeholder="請猜四位 0~9 的數字"
        />
        <button>確認猜測</button>
      </form>
      <h3>{guess}</h3>
    </div>
  );
}
